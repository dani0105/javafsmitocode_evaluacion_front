import { SignoService } from './../../../_service/signo.service';
import { Component, OnInit } from '@angular/core';
import { Paciente } from '../../../_model/paciente';
import { MatDialogRef } from '../../../../../node_modules/@angular/material';
import { PacienteService } from '../../../_service/paciente.service';

@Component({
  selector: 'app-dialogo-paciente',
  templateUrl: './dialogo-paciente.component.html',
  styleUrls: ['./dialogo-paciente.component.css']
})
export class DialogoPacienteComponent implements OnInit {

  paciente: Paciente;

  constructor(private dialogRef: MatDialogRef<DialogoPacienteComponent>, 
              private pacienteService: PacienteService, 
              private signoService: SignoService) { }

  ngOnInit() {
    //this.paciente = this.data;
    
    this.paciente = new Paciente();
  }

  cancelar() {
    this.dialogRef.close();
  }

  operar() {

      this.pacienteService.registrar(this.paciente).subscribe(data => {        
          this.pacienteService.listar().subscribe(pacientes => {
            this.pacienteService.pacienteCambio.next(pacientes);
            this.signoService.pacienteCambio.next(pacientes);
            this.signoService.mensajeCambio.next("Se registro");
          });        
      });
    
    this.dialogRef.close();
  }

}
