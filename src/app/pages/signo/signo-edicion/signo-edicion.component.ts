import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '../../../../../node_modules/@angular/forms';
import { Paciente } from '../../../_model/paciente';
import { Observable } from '../../../../../node_modules/rxjs';
import { PacienteService } from '../../../_service/paciente.service';
import { MatSnackBar, MatDialog } from '../../../../../node_modules/@angular/material';
import { Signo } from '../../../_model/signo';
import { SignoService } from '../../../_service/signo.service';
import { map } from '../../../../../node_modules/rxjs/operators';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { DialogoPacienteComponent } from '../dialogo-paciente/dialogo-paciente.component';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  form: FormGroup;

  id: number;
  myControlPaciente: FormControl = new FormControl();
  pacientes: Paciente[] = [];
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date()
  mensaje: string;
  filteredOptions: Observable<any[]>;
  signo: Signo;
  pacienteSeleccionado: Paciente;
  temperatura: string;
  pulso: string;
  ritmoRespiratorio: string;
  edicion: boolean;

  constructor(private route: ActivatedRoute,
            private builder: FormBuilder, 
            private dialog: MatDialog,
            private pacienteService: PacienteService, 
            private signoService: SignoService,
            private router: Router, 
            public snackBar: MatSnackBar) {

    this.form = builder.group({
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl('')
    });

  }

  ngOnInit() {
    this.listarPacientes();
    this.route.params.subscribe( (params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.signo = new Signo();
      this.initForm();
    });
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
  }

  initForm(){
    if(this.edicion){
      //cargar la data del servicio en el form
      
      this.signoService.listarSignoPorId(this.id).subscribe(data => {
        this.signo.idSigno = data.idSigno;
        //this.myControlPaciente = new FormControl(data.paciente);
        this.myControlPaciente.setValue(data.paciente);
        this.pacienteSeleccionado = data.paciente;
        this.form = new FormGroup({
          'paciente' : this.myControlPaciente,
          'fecha' : new FormControl(new Date(data.fecha)),
          'temperatura' : new FormControl(data.temperatura),
          'pulso' : new FormControl(data.pulso),
          'ritmoRespiratorio' : new FormControl(data.ritmoRespiratorio)
        });
      });
    } else {
      this.myControlPaciente.setValue(this.pacientes[1]);
      this.form = new FormGroup({
        'paciente' : this.myControlPaciente,
        'fecha' : new FormControl(new Date()),
        'temperatura' : new FormControl(''),
        'pulso' : new FormControl(''),
        'ritmoRespiratorio' : new FormControl('')
      });
    }


    //this.pacienteSeleccionado = this.form.value['paciente'];
    //this.fechaSeleccionada = this.form.value['fecha'];
    //this.temperatura = this.form.value['temperatura'];
    //this.pulso = this.form.value['pulso'];
    //this.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];

  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
    this.signoService.pacienteCambio.subscribe(data => {
      this.pacientes = data;
    })
  }

  seleccionarPaciente(e: any){
    //console.log(e);
    this.pacienteSeleccionado = e.option.value;
  }

  estadoBotonRegistrar() {
    return (this.pacienteSeleccionado === null 
        || (this.temperatura === null)
        || (this.pulso === null)
        || (this.ritmoRespiratorio === null));
  }


  aceptar() {

    if(this.myControlPaciente.value != undefined && this.form.value['temperatura'] != undefined && this.form.value['pulso'] != undefined && this.form.value['ritmoRespiratorio'] != undefined )
    //if(this.myControlPaciente.value != undefined && this.temperatura != undefined && this.pulso != undefined && this.ritmoRespiratorio != undefined )    
    {
      this.signo.paciente = this.form.value['paciente']; //this.pacienteSeleccionado;
      this.signo.temperatura = this.form.value['temperatura'];//this.temperatura;
      this.signo.pulso = this.form.value['pulso'];//this.pulso;
      this.signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];//this.ritmoRespiratorio;
      var tzoffset = (this.form.value['fecha']).getTimezoneOffset() * 60000;
      var localISOTime = (this.form.value['fecha']).toISOString();
      //var localISOTime = (new Date(Date.now() - tzoffset)).toISOString();
      //var localISOTime = (new Date(tzoffset)).toISOString()
      this.signo.fecha = localISOTime;
    } else {
      this.mensaje = `Debe agregar un paciente, temperatura, pulso y ritmo respiratorio`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
      return;
    }  

    
    console.log(this.signo);

    if(this.edicion){
      //actualizar
      this.signoService.modificar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se modificó');
        });
      });
    }else{
      //registrar
      this.signoService.registrar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se registró');
        });
      });
    }this.router.navigate(['signo']);
  }

  limpiarControles() {
    this.signo = new Signo();
    this.pacienteSeleccionado = null;
    this.fechaSeleccionada = null;
    this.temperatura = null;
    this.pulso = null;
    this.ritmoRespiratorio = null;

    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.mensaje = '';
  }

  dialogNuevoPaciente(){
    this.dialog.open(DialogoPacienteComponent, {
      width: '250px',
      disableClose: true,
    })
  }
}
